#include <stdio.h> 
#include <string.h> 
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h> 
  
int main() 
{ 
    int fd1; 
  
    char * data = "/tmp/data"; 

    mkfifo(data, 0666); 
  
    char str1[150], str2[150]; 
    while (1) 
    { 
        fd1 = open(data,O_RDONLY); 
        read(fd1, str1, 150); 
  
        printf("From Client: %s\n", str1); 
        close(fd1); 
  
        fd1 = open(data,O_WRONLY); 
	scanf("%s", str2);

        write(fd1, str2, strlen(str2)+1); 
        close(fd1); 
    } 
    return 0; 
} 
